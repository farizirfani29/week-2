//  ================================= math ====================

let result = document.getElementById('result');
let expression = '';

function inputNum(value) {
    expression += value;
    result.value = expression;
}

function appendOperator(operator) {
    expression += operator;
    result.value = expression;
}

function clearResult() {
    expression = '';
    result.value = '';
}

function del() {
    expression = expression.slice(0, -1)
    result.value = expression;
}

function calculate() {
    try {
        const evaluated = eval(expression);
        result.value = evaluated;
        expression = evaluated.toString();
    } catch (error) {
        result.value = 'Error';
    }
}