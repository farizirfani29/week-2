let toggleSwitch = document.getElementsByClassName('redButton')[0]

function go_to_1() {
    toggleSwitch.classList.add('horizTranslate1');
    toggleSwitch.classList.remove('horizTranslate2');
    toggleSwitch.classList.remove('horizTranslate3');
    document.getElementById("main").style.backgroundColor = "#3b4664"
    document.getElementById("buttonContainer").style.backgroundColor = "#222D41"
    document.getElementById("legendTextContainer").style.color = "#ffffff"
    document.getElementById("text").style.color = "#ffffff"
    document.getElementById("screen").style.backgroundColor = "#181e33"
    document.getElementById("box-btn").style.backgroundColor = "#242d45"

    // screnn
    document.getElementById('result').style.color = '#ffffff'


    // btn number background
    document.getElementById('btn-num-1').style.backgroundColor = '#ebe2dd'
    document.getElementById('btn-num-2').style.backgroundColor = '#ebe2dd'
    document.getElementById('btn-num-3').style.backgroundColor = '#ebe2dd'
    document.getElementById('btn-num-4').style.backgroundColor = '#ebe2dd'
    document.getElementById('btn-num-5').style.backgroundColor = '#ebe2dd'
    document.getElementById('btn-num-6').style.backgroundColor = '#ebe2dd'
    document.getElementById('btn-num-7').style.backgroundColor = '#ebe2dd'
    document.getElementById('btn-num-8').style.backgroundColor = '#ebe2dd'
    document.getElementById('btn-num-9').style.backgroundColor = '#ebe2dd'
    document.getElementById('btn-num-0').style.backgroundColor = '#ebe2dd'
    document.getElementById('btn-plus').style.backgroundColor = '#ebe2dd'
    document.getElementById('btn-min').style.backgroundColor = '#ebe2dd'
    document.getElementById('btn-dot').style.backgroundColor = '#ebe2dd'
    document.getElementById('btn-bagi').style.backgroundColor = '#ebe2dd'
    document.getElementById('btn-kali').style.backgroundColor = '#ebe2dd'
    document.getElementById('btn-del').style.backgroundColor = '#62729a'
    document.getElementById('btn-reset').style.backgroundColor = '#62729a'
    document.getElementById('btn-equal').style.backgroundColor = '#d13f30'

    // btn number color
    document.getElementById('btn-num-1').style.color = '#414450'
    document.getElementById('btn-num-2').style.color = '#414450'
    document.getElementById('btn-num-3').style.color = '#414450'
    document.getElementById('btn-num-4').style.color = '#414450'
    document.getElementById('btn-num-5').style.color = '#414450'
    document.getElementById('btn-num-6').style.color = '#414450'
    document.getElementById('btn-num-7').style.color = '#414450'
    document.getElementById('btn-num-8').style.color = '#414450'
    document.getElementById('btn-num-9').style.color = '#414450'
    document.getElementById('btn-num-0').style.color = '#414450'
    document.getElementById('btn-plus').style.color = '#414450'
    document.getElementById('btn-min').style.color = '#414450'
    document.getElementById('btn-dot').style.color = '#414450'
    document.getElementById('btn-bagi').style.color = '#414450'
    document.getElementById('btn-kali').style.color = '#414450'
    document.getElementById('btn-del').style.color = '#fdffff'
    document.getElementById('btn-reset').style.color = '#fdffff'
    document.getElementById('btn-equal').style.color = '#fdffff'

    //btn shadow color

    document.getElementById('btn-num-1').style.borderBottom = '4px solid #a89d94'
    document.getElementById('btn-num-2').style.borderBottom = '4px solid #a89d94'
    document.getElementById('btn-num-3').style.borderBottom = '4px solid #a89d94'
    document.getElementById('btn-num-4').style.borderBottom = '4px solid #a89d94'
    document.getElementById('btn-num-5').style.borderBottom = '4px solid #a89d94'
    document.getElementById('btn-num-6').style.borderBottom = '4px solid #a89d94'
    document.getElementById('btn-num-7').style.borderBottom = '4px solid #a89d94'
    document.getElementById('btn-num-8').style.borderBottom = '4px solid #a89d94'
    document.getElementById('btn-num-9').style.borderBottom = '4px solid #a89d94'
    document.getElementById('btn-num-0').style.borderBottom = '4px solid #a89d94'
    document.getElementById('btn-plus').style.borderBottom = '4px solid #a89d94'
    document.getElementById('btn-min').style.borderBottom = '4px solid #a89d94'
    document.getElementById('btn-dot').style.borderBottom = '4px solid #a89d94'
    document.getElementById('btn-bagi').style.borderBottom = '4px solid #a89d94'
    document.getElementById('btn-kali').style.borderBottom = '4px solid #a89d94'
    document.getElementById('btn-del').style.borderBottom = '4px solid #48547c'
    document.getElementById('btn-reset').style.borderBottom = '4px solid #48547c'
    document.getElementById('btn-equal').style.borderBottom = '4px solid #88271a'
}

function go_to_2() {
    toggleSwitch.classList.add('horizTranslate2');
    toggleSwitch.classList.remove('horizTranslate3');
    toggleSwitch.classList.remove('horizTranslate1');
    document.getElementById("main").style.backgroundColor = "#e6e6e6"
    document.getElementById("buttonContainer").style.backgroundColor = "#D3CCCA"
    document.getElementById("legendTextContainer").style.color = "#393830"
    document.getElementById("text").style.color = "#393830"
    document.getElementById("screen").style.backgroundColor = "#eeeeee"
    document.getElementById("box-btn").style.backgroundColor = "#d3ccce"

    // screen
    document.getElementById('result').style.color = '#393931'

    // btn number background
    document.getElementById('btn-num-1').style.backgroundColor = '#e5e4e0'
    document.getElementById('btn-num-2').style.backgroundColor = '#e5e4e0'
    document.getElementById('btn-num-3').style.backgroundColor = '#e5e4e0'
    document.getElementById('btn-num-4').style.backgroundColor = '#e5e4e0'
    document.getElementById('btn-num-5').style.backgroundColor = '#e5e4e0'
    document.getElementById('btn-num-6').style.backgroundColor = '#e5e4e0'
    document.getElementById('btn-num-7').style.backgroundColor = '#e5e4e0'
    document.getElementById('btn-num-8').style.backgroundColor = '#e5e4e0'
    document.getElementById('btn-num-9').style.backgroundColor = '#e5e4e0'
    document.getElementById('btn-num-0').style.backgroundColor = '#e5e4e0'
    document.getElementById('btn-plus').style.backgroundColor = '#e5e4e0'
    document.getElementById('btn-min').style.backgroundColor = '#e5e4e0'
    document.getElementById('btn-dot').style.backgroundColor = '#e5e4e0'
    document.getElementById('btn-bagi').style.backgroundColor = '#e5e4e0'
    document.getElementById('btn-kali').style.backgroundColor = '#e5e4e0'
    document.getElementById('btn-del').style.backgroundColor = '#357f88'
    document.getElementById('btn-reset').style.backgroundColor = '#357f88'
    document.getElementById('btn-equal').style.backgroundColor = '#c95501'

    // btn number color
    document.getElementById('btn-num-1').style.color = '#32332b'
    document.getElementById('btn-num-2').style.color = '#32332b'
    document.getElementById('btn-num-3').style.color = '#32332b'
    document.getElementById('btn-num-4').style.color = '#32332b'
    document.getElementById('btn-num-5').style.color = '#32332b'
    document.getElementById('btn-num-6').style.color = '#32332b'
    document.getElementById('btn-num-7').style.color = '#32332b'
    document.getElementById('btn-num-8').style.color = '#32332b'
    document.getElementById('btn-num-9').style.color = '#32332b'
    document.getElementById('btn-num-0').style.color = '#32332b'
    document.getElementById('btn-plus').style.color = '#32332b'
    document.getElementById('btn-min').style.color = '#32332b'
    document.getElementById('btn-dot').style.color = '#32332b'
    document.getElementById('btn-bagi').style.color = '#32332b'
    document.getElementById('btn-kali').style.color = '#32332b'
    document.getElementById('btn-del').style.color = '#fdffff'
    document.getElementById('btn-reset').style.color = '#fdffff'
    document.getElementById('btn-equal').style.color = '#fdffff'

    //btn shadow color

    document.getElementById('btn-num-1').style.borderBottom = '4px solid #b1a599'
    document.getElementById('btn-num-2').style.borderBottom = '4px solid #b1a599'
    document.getElementById('btn-num-3').style.borderBottom = '4px solid #b1a599'
    document.getElementById('btn-num-4').style.borderBottom = '4px solid #b1a599'
    document.getElementById('btn-num-5').style.borderBottom = '4px solid #b1a599'
    document.getElementById('btn-num-6').style.borderBottom = '4px solid #b1a599'
    document.getElementById('btn-num-7').style.borderBottom = '4px solid #b1a599'
    document.getElementById('btn-num-8').style.borderBottom = '4px solid #b1a599'
    document.getElementById('btn-num-9').style.borderBottom = '4px solid #b1a599'
    document.getElementById('btn-num-0').style.borderBottom = '4px solid #b1a599'
    document.getElementById('btn-plus').style.borderBottom = '4px solid #b1a599'
    document.getElementById('btn-min').style.borderBottom = '4px solid #b1a599'
    document.getElementById('btn-dot').style.borderBottom = '4px solid #b1a599'
    document.getElementById('btn-bagi').style.borderBottom = '4px solid #b1a599'
    document.getElementById('btn-kali').style.borderBottom = '4px solid #b1a599'
    document.getElementById('btn-del').style.borderBottom = '4px solid #255b62'
    document.getElementById('btn-reset').style.borderBottom = '4px solid #255b62'
    document.getElementById('btn-equal').style.borderBottom = '4px solid #88271a'

}

function go_to_3() {
    toggleSwitch.classList.add('horizTranslate3');
    toggleSwitch.classList.remove('horizTranslate2');
    toggleSwitch.classList.remove('horizTranslate1');
    document.getElementById("main").style.backgroundColor = "#170726"
    document.getElementById("buttonContainer").style.backgroundColor = "#1e0836"
    document.getElementById("text").style.color = "#E2D241"
    document.getElementById("screen").style.backgroundColor = "#1f0936"
    document.getElementById("box-btn").style.backgroundColor = "#1f0936"

    //
    document.getElementById('result').style.color = '#E2D241'

    // btn number background
    document.getElementById('btn-num-1').style.backgroundColor = '#331b4d'
    document.getElementById('btn-num-2').style.backgroundColor = '#331b4d'
    document.getElementById('btn-num-3').style.backgroundColor = '#331b4d'
    document.getElementById('btn-num-4').style.backgroundColor = '#331b4d'
    document.getElementById('btn-num-5').style.backgroundColor = '#331b4d'
    document.getElementById('btn-num-6').style.backgroundColor = '#331b4d'
    document.getElementById('btn-num-7').style.backgroundColor = '#331b4d'
    document.getElementById('btn-num-8').style.backgroundColor = '#331b4d'
    document.getElementById('btn-num-9').style.backgroundColor = '#331b4d'
    document.getElementById('btn-num-0').style.backgroundColor = '#331b4d'
    document.getElementById('btn-plus').style.backgroundColor = '#331b4d'
    document.getElementById('btn-min').style.backgroundColor = '#331b4d'
    document.getElementById('btn-dot').style.backgroundColor = '#331b4d'
    document.getElementById('btn-bagi').style.backgroundColor = '#331b4d'
    document.getElementById('btn-kali').style.backgroundColor = '#331b4d'
    document.getElementById('btn-del').style.backgroundColor = '#56067d'
    document.getElementById('btn-reset').style.backgroundColor = '#56067d'
    document.getElementById('btn-equal').style.backgroundColor = '#00decf'

    // btn number color
    document.getElementById('btn-num-1').style.color = '#E2D241'
    document.getElementById('btn-num-2').style.color = '#E2D241'
    document.getElementById('btn-num-3').style.color = '#E2D241'
    document.getElementById('btn-num-4').style.color = '#E2D241'
    document.getElementById('btn-num-5').style.color = '#E2D241'
    document.getElementById('btn-num-6').style.color = '#E2D241'
    document.getElementById('btn-num-7').style.color = '#E2D241'
    document.getElementById('btn-num-8').style.color = '#E2D241'
    document.getElementById('btn-num-9').style.color = '#E2D241'
    document.getElementById('btn-num-0').style.color = '#E2D241'
    document.getElementById('btn-plus').style.color = '#E2D241'
    document.getElementById('btn-min').style.color = '#E2D241'
    document.getElementById('btn-dot').style.color = '#E2D241'
    document.getElementById('btn-bagi').style.color = '#E2D241'
    document.getElementById('btn-kali').style.color = '#E2D241'
    document.getElementById('btn-del').style.color = '#fdffff'
    document.getElementById('btn-reset').style.color = '#fdffff'
    document.getElementById('btn-equal').style.color = '#004242'

    //btn shadow color

    document.getElementById('btn-num-1').style.borderBottom = '4px solid #802394'
    document.getElementById('btn-num-2').style.borderBottom = '4px solid #802394'
    document.getElementById('btn-num-3').style.borderBottom = '4px solid #802394'
    document.getElementById('btn-num-4').style.borderBottom = '4px solid #802394'
    document.getElementById('btn-num-5').style.borderBottom = '4px solid #802394'
    document.getElementById('btn-num-6').style.borderBottom = '4px solid #802394'
    document.getElementById('btn-num-7').style.borderBottom = '4px solid #802394'
    document.getElementById('btn-num-8').style.borderBottom = '4px solid #802394'
    document.getElementById('btn-num-9').style.borderBottom = '4px solid #802394'
    document.getElementById('btn-num-0').style.borderBottom = '4px solid #802394'
    document.getElementById('btn-plus').style.borderBottom = '4px solid #802394'
    document.getElementById('btn-min').style.borderBottom = '4px solid #802394'
    document.getElementById('btn-dot').style.borderBottom = '4px solid #802394'
    document.getElementById('btn-bagi').style.borderBottom = '4px solid #802394'
    document.getElementById('btn-kali').style.borderBottom = '4px solid #802394'
    document.getElementById('btn-del').style.borderBottom = '4px solid #ac23e3'
    document.getElementById('btn-reset').style.borderBottom = '4px solid #ac23e3'
    document.getElementById('btn-equal').style.borderBottom = '4px solid #6ffbf2'
}